# A Vite-react project showcasing what ThreeJS is capable of

With NPM:

$ npm create vite@latest
With Yarn:

$ yarn create vite
With PNPM:

$ pnpm create vite
Then follow the prompts!

You can also directly specify the project name and the template you want to use via additional command line options. For example, to scaffold a Vite + Vue project, run:

# npm 6.x
npm create vite@latest my-vue-app --template react

# npm 7+, extra double-dash is needed:
npm create vite@latest my-vue-app -- --template react

# yarn
yarn create vite my-vue-app --template react

# pnpm
pnpm create vite my-vue-app --template react

To run:
npm install
npx vite